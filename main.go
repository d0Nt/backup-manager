package main

import (
	"log"
	"os"
)

func main() {
	file, err := os.OpenFile("app.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	log.SetOutput(file)

	app()
}

func app() {
	conf := readConfig()

	if !isFolderExist(conf.BackupRootDir) {
		createFolder(conf.BackupRootDir, conf.MaskForFolders)
	}
	setupBackupFolders(conf.BackupRootDir, conf.BackupSetups, conf.MaskForFolders)
	setupRetentionFolders(conf.BackupRootDir, conf.BackupSetups, conf.MaskForFolders)

	for _, backup := range conf.BackupSetups {
		backup.BackupFileName = generateBackupFileName(backup.Name, conf.DateFormat)
		createBackup(conf.BackupRootDir, backup)

		executeRetentionRules(conf.BackupRootDir, backup, conf.DateFormat)

		if backup.Bucket.Endpoint != "" {
			syncToBucket(backup, conf.BackupRootDir)
		}
	}
}
