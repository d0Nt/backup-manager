package main

import (
	"log"
	"os"
	"regexp"
	"strings"

	"gopkg.in/yaml.v2"
)

type Config struct {
	BackupRootDir  string        `yaml:"backups_directory"`
	MaskForFolders os.FileMode   `yaml:"directory_mask"`
	DateFormat     string        `yaml:"date_format"`
	BackupSetups   []BackupSetup `yaml:"backups"`
}

func readConfig() Config {
	yamlFile, err := os.ReadFile("config.yml")
	if err != nil {
		log.Fatalf("Failed to read ./config.yml. Create one if you forgot: %v", err)
	}

	var config Config
	err = yaml.Unmarshal(yamlFile, &config)
	if err != nil {
		log.Fatalf("Failed to parse config.yml. Check file format: %v", err)
	}

	isConfigValid(config)

	return config
}

func isConfigValid(config Config) bool {
	namePattern := "^[a-zA-Z_]+$"
	regex := regexp.MustCompile(namePattern)

	if strings.Contains(config.DateFormat, ".") {
		log.Fatalln("Date format should not include dots")
		return false
	}

	for _, backup := range config.BackupSetups {
		if !regex.MatchString(backup.Name) {
			log.Fatalf("%s backup_name is invalid\n", backup.Name)
			return false
		}
	}
	return true
}
