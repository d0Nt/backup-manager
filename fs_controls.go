package main

import (
	"io"
	"io/fs"
	"log"
	"os"
	"path/filepath"
)

func isFolderExist(folderPath string) bool {
	_, err := os.Stat(folderPath)
	if os.IsPermission(err) {
		log.Println("Failed to check if folder exists. Error: ", err)
	}

	return !os.IsNotExist(err)
}

func createFolder(folderPath string, mask fs.FileMode) {
	err := os.MkdirAll(folderPath, mask)
	if err != nil {
		log.Fatalln("Error creating folder:", err)
		return
	}
}

func getFilesInFolder(folderPath string) []string {
	var files []string
	err := filepath.Walk(folderPath,
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if info.IsDir() {
				return nil
			}

			files = append(files, path)
			return nil
		})

	if err != nil {
		log.Fatalf("Error opening directory: %s", err)
	}

	return files
}

func copyFile(srcPath string, destPath string) {
	sourceFileStat, err := os.Stat(srcPath)
	if err != nil {
		log.Fatalf(err.Error())
	}

	if !sourceFileStat.Mode().IsRegular() {
		log.Fatalf("%s is not a regular file", srcPath)
	}

	source, err := os.Open(srcPath)
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer source.Close()

	destination, err := os.Create(destPath)
	if err != nil {
		log.Fatalf(err.Error())
	}
	defer destination.Close()
	_, err = io.Copy(destination, source)

	if err != nil {
		log.Fatalf(err.Error())
	}
}

func deleteFile(filePath string) {
	err := os.Remove(filePath)
	if err != nil {
		log.Fatalf(err.Error())
	}

}
