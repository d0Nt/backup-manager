package main

import (
	"context"
	"log"
	"os"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/credentials"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
)

type BucketSetup struct {
	Type       string `yaml:"type"`
	Name       string `yaml:"name"`
	FilePrefix string `yaml:"file_prefix"`
	KeyId      string `yaml:"key_id"`
	Key        string `yaml:"key"`
	Endpoint   string `yaml:"endpoint"`
	Region     string `yaml:"region"`
}

func getBucketClient(bucket BucketSetup) *s3.Client {
	configD := aws.Config{
		Credentials:  credentials.NewStaticCredentialsProvider(bucket.KeyId, bucket.Key, ""),
		BaseEndpoint: aws.String(bucket.Endpoint),
		Region:       bucket.Region,
	}
	return s3.NewFromConfig(configD)
}

func getObjectsInBucket(bucket BucketSetup, directory string) []string {
	var filesInBucket []string
	client := getBucketClient(bucket)

	result, err := client.ListObjectsV2(context.TODO(), &s3.ListObjectsV2Input{
		Bucket: aws.String(bucket.Name),
		Prefix: aws.String(directory),
	})
	if err != nil {
		log.Fatalf("Couldn't get objects in bucket %v. Reason: %v\n", bucket.Name, err)
		return filesInBucket
	}

	for _, obj := range result.Contents {
		filesInBucket = append(filesInBucket, *obj.Key)
	}

	return filesInBucket
}

func deleteFilesFromBucket(bucket BucketSetup, objectPaths []string) bool {
	client := getBucketClient(bucket)

	var objectIds []types.ObjectIdentifier
	for _, key := range objectPaths {
		objectIds = append(objectIds, types.ObjectIdentifier{Key: aws.String(key)})
	}

	output, err := client.DeleteObjects(context.TODO(), &s3.DeleteObjectsInput{
		Bucket: aws.String(bucket.Name),
		Delete: &types.Delete{Objects: objectIds},
	})
	if err != nil {
		log.Printf("Couldn't delete objects from bucket %v. Reason: %v\n", bucket.Name, err)
		return false
	}

	log.Printf("Deleted %v objects.\n", len(output.Deleted))
	return true
}

func uploadFileToBucket(bucket BucketSetup, filePath string, objectPathInBucket string) bool {
	client := getBucketClient(bucket)

	file, err := os.Open(filePath)
	if err != nil {
		log.Printf("Couldn't open file %v to upload. Reason: %v\n", filePath, err)
		return false
	}

	defer file.Close()
	_, err = client.PutObject(context.TODO(), &s3.PutObjectInput{
		Bucket: aws.String(bucket.Name),
		Key:    aws.String(objectPathInBucket),
		Body:   file,
	})
	if err != nil {
		log.Printf("Couldn't upload file %v to %v:%v. Reason: %v\n",
			filePath, bucket.Name, objectPathInBucket, err)
		return false
	}
	log.Printf("Uploaded %v to %v:%v.\n",
		filePath, bucket.Name, objectPathInBucket)
	return true
}
