package main

import (
	"io/fs"
	"log"
	"path"
	"sort"
	"strings"
	"time"
)

type BackupSetup struct {
	Name               string           `yaml:"name"`
	Type               string           `yaml:"type"`
	Host               string           `yaml:"host"`
	Port               int              `yaml:"port"`
	User               string           `yaml:"user"`
	Password           string           `yaml:"password"`
	DumpAll            bool             `yaml:"dump_all"`
	DocerContainerName string           `yaml:"docker_container_name"`
	UseGzip            bool             `yaml:"use_gzip"`
	Databases          []string         `yaml:"databases"`
	Retention          []RetentionSetup `yaml:"retention"`
	Bucket             BucketSetup      `yaml:"bucket"`
	BackupFileName     string
}

type Backup struct {
	FileName string
	Date     time.Time
}

func setupBackupFolders(rootDir string, setups []BackupSetup, mask fs.FileMode) {
	for _, setup := range setups {
		path := path.Join(rootDir, setup.Name)
		if isFolderExist(path) {
			continue
		}

		log.Printf("Creating folder %s(%s)\n", path, mask)
		createFolder(path, mask)
	}
}

func createBackup(rootDir string, backup BackupSetup) {
	switch backup.Type {
	case "mariadb":
		createMariaDbBackup(rootDir, backup)
	default:
		log.Println("Unsupported backup type: " + backup.Type)
	}
}

func generateBackupFileName(name string, dateFormat string) string {
	return "backup_" + name + "_" + time.Now().Format(dateFormat)
}

func getDateFromBackupName(fullName string, dateFormat string) time.Time {
	fileName := strings.Split(fullName, ".")[0]
	dateString := fileName[len(fileName)-len(dateFormat):]
	parsedTime, err := time.Parse(dateFormat, dateString)
	if err != nil {
		log.Fatalf("Error parsing file name %s to date: %s\n", fileName, err)
	}
	return parsedTime
}

func getBackupFiles(entries []string, dateFormat string) []Backup {
	var backupFiles []Backup

	for _, entry := range entries {
		backupName := entry
		backupDate := getDateFromBackupName(backupName, dateFormat)
		backupFiles = append(backupFiles, Backup{FileName: backupName, Date: backupDate})
	}

	return backupFiles
}

func sortByDate(backupFiles []Backup) {
	sort.Slice(backupFiles, func(i, j int) bool {
		return backupFiles[j].Date.Before(backupFiles[i].Date)
	})
}

func getSortedBackups(entries []string, dateFormat string) []Backup {
	backupFiles := getBackupFiles(entries, dateFormat)
	sortByDate(backupFiles)
	return backupFiles
}
