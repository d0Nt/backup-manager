# 📖 Backup manager
Backup manager is tool to automate backup management process. It not only backs up your database but also implements rotation policies for efficient storage management. Additionally, it seamlessly uploads backups to your AWS S3 bucket for off-site storage, providing redundancy and disaster recovery capabilities.

## 🛠 Built With

 * [GO](https://go.dev/)
 * [aws-sdk-go-v2](https://github.com/aws/aws-sdk-go-v2)
 * [YAML for GO](https://github.com/go-yaml/yaml)

### Key Features
 * Executes backup scripts. Currently supported: mariadb, mariadb docker container;
 * Rotates backup files using defined rules;
 * Uploads changes to s3 bucket. AWS and Backblaze should be supported;

## 💻 Getting Started
 1. Install [GO](https://go.dev/);
 2. Clone this repository;
 3. Open repository `cd backup-manager`
 4. Install required go packages `go install`
 5. Build `go build`
 6. Create config from example `example.config.yml`
 7. Upload in appropriate server
 8. Add build output script(`backup_manager` by default) to server cronjob `crontab -e`

Example crontab line:
```sh
0 3 * * * (cd /opt/backup_manager && exec ./backup_manager)
```

## ⚙ Config options

### backups_directory
`backups_directory` option defines where to store local copies of backups. Full path can be provided as well as relative.

Examples:
```yml
backups_directory: "./backups"
```
```yml
backups_directory: "/var/backups"
```

### directory_mask
`directory_mask` permissions for directories created by script.

```yml
directory_mask: 0750
```

### date_format
`date_format` provides date format to be used in file names.

```yml
date_format: "2006-01-02_15-04-05"
```

### Other options
`backups` provides list of backups to execute.

```yml
backups:
    - name: web_backup # Backup name. Will be used as local parent directory too.
      type: mariadb    # Implemented backup type
      host: localhost  # host paramter for `type` backup script. For mariadb: mariadb-dump -h option
      port: 3306       # port paramter for `type` backup script. For mariadb: mariadb-dump -P option
      user: user       # user paramter for `type` backup script. For mariadb: mariadb-dump -u option
      password: "password" # user password paramter for `type` backup script. For mariadb: mariadb-dump -p option
      use_gzip: true # use gzip to compress output file
      dump_all: false # dump all databases or not
      docker_container_name: "mariadb" # if using docker, container name should be provided here
      databases: # If not dumping all databases, provide database list to backup
        - web_db
      retention:                            # file rotation rules
        - name: daily                       # rotation rule name
          storage_count: 7                  # how much different files should be stored
          min_hours_between_backups: 12     # if difference between last saved backup and current date will be lower than this number - file will not be saved for this rule
          max_hours_between_backups: 24     # ignore other rules if time exeeds provided amount
        - name: weekly
          storage_count: 4
          min_hours_between_backups: 24
          max_hours_between_backups: 168
          execute_week_day: 1               # execute rule when week day is provided number. 1 for monday
        - name: monthly
          storage_count: 12
          min_hours_between_backups: 24
          max_hours_between_backups: 744
          execute_month_day: 1              # execute rule when month day is provided number. 1 for first day of the month
        - name: yearly
          storage_count: 5
          min_hours_between_backups: 4320
          max_hours_between_backups: 8784
          execute_month_day: 1
          execute_month: 1                  # execute rule when month is provided number. 1 for January


      bucket:
        type: s3                            # bucket type. Only s3 is supported
        name: bucket-name                   # bucket name
        file_prefix: bucket_file_pefix      # bucket file prefix(folder)
        key_id: key_id                      # bucket access key id
        key: access_key                     # bucket access key
        endpoint: https://s3.region.provider.com    # bucket endpoin
        region: region                              # bucket region
```

`bucket` and `retention` options are optional as well as some `backup`.

# License
[MIT](LICENSE)