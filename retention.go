package main

import (
	"log"
	"os"
	"path"
	"path/filepath"
	"time"
)

type RetentionSetup struct {
	Name                   string `yaml:"name"`
	BackupCount            int    `yaml:"storage_count"`
	MinHoursBetweenBackups int    `yaml:"min_hours_between_backups"`
	MaxHoursBetweenBackups int    `yaml:"max_hours_between_backups"`
	ExecuteOnMonthDay      int    `yaml:"execute_month_day"`
	ExecuteOnWeekday       int    `yaml:"execute_week_day"`
	ExecuteOnMonth         int    `yaml:"execute_month"`
}

func setupRetentionFolders(rootDir string, databases []BackupSetup, mask os.FileMode) {
	for _, backup := range databases {
		for _, retention := range backup.Retention {
			path := path.Join(rootDir, backup.Name, retention.Name)
			if isFolderExist(path) {
				continue
			}

			log.Printf("Creating folder %s(%s)\n", path, mask)
			createFolder(path, mask)
		}
	}
}

func executeRetentionRules(rootDir string, backup BackupSetup, dateFormat string) {
	searchFileName := path.Join(rootDir, backup.Name, backup.BackupFileName+".*")
	matches, err := filepath.Glob(searchFileName)
	if err != nil {
		log.Printf("Failed to execute retention rules. Error: %s\n", err)
		return
	}
	if len(matches) < 1 {
		log.Printf("Failed to execute retention rules. Backup file was not found %s\n", backup.BackupFileName)
		return
	}
	if len(matches) > 1 {
		log.Printf("Failed to execute retention rules. Found more than one file with name %s\n", backup.BackupFileName)
		return
	}

	backup.BackupFileName = path.Base(matches[0])

	for _, rule := range backup.Retention {
		executeRetentionRule(rootDir, backup.Name, backup.BackupFileName, rule, dateFormat)
	}

	if len(backup.Retention) > 1 {
		deleteFile(path.Join(rootDir, backup.Name, backup.BackupFileName))
	}
}

func executeRetentionRule(rootDir string, backupName string, newBackupFile string, setup RetentionSetup, dateFormat string) {
	log.Printf("Executing %s retention rule\n", setup.Name)
	folderPath := filepath.Join(rootDir, backupName, setup.Name)

	filesInFolder := getFilesInFolder(folderPath)
	sortedBackupList := getSortedBackups(filesInFolder, dateFormat)

	lastBackupHoursAgo := 0

	if len(sortedBackupList) > 0 {
		lastBackupHoursAgo = int(time.Since(sortedBackupList[0].Date).Hours())
	}

	if len(sortedBackupList) < 1 || isRetentionConditionMet(setup) && lastBackupHoursAgo > setup.MinHoursBetweenBackups || lastBackupHoursAgo > setup.MaxHoursBetweenBackups {
		srcPath := filepath.Join(rootDir, backupName, newBackupFile)
		destPath := filepath.Join(rootDir, backupName, setup.Name, newBackupFile)
		copyFile(srcPath, destPath)
	}

	deleteOldBackupFiles(folderPath, setup.BackupCount, dateFormat)
}

func isRetentionConditionMet(setup RetentionSetup) bool {
	timeNow := time.Now()
	if setup.ExecuteOnMonth != 0 && setup.ExecuteOnMonth != int(timeNow.Month()) {
		return false
	}
	if setup.ExecuteOnMonthDay != 0 && setup.ExecuteOnMonthDay != int(timeNow.Day()) {
		return false
	}
	if setup.ExecuteOnWeekday != 0 && setup.ExecuteOnWeekday != int(timeNow.Weekday()) {
		return false
	}
	return true
}

func deleteOldBackupFiles(folderPath string, count int, dateFormat string) {
	filesInFolder := getFilesInFolder(folderPath)
	sortedBackupList := getSortedBackups(filesInFolder, dateFormat)

	if len(sortedBackupList) < count {
		return
	}

	for i, backup := range sortedBackupList {
		log.Println(backup.Date)
		if i >= count {
			deleteFile(backup.FileName)
		}
	}
}
