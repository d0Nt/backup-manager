package main

import (
	"log"
	"os/exec"
	"path"
	"strconv"
	"strings"
)

func createMariaDbBackup(rootDir string, backup BackupSetup) string {
	fileExtension := ".sql"
	outputDirectory := path.Join(rootDir, backup.Name)

	//databases
	var databases string
	if backup.DumpAll {
		databases = "--all-databases"
	} else {
		databases = strings.Join(backup.Databases, " ")
	}

	//archive
	var archiveMethod string
	if backup.UseGzip {
		archiveMethod = " | gzip > "
		fileExtension += ".gz"
	} else {
		archiveMethod = " > "
	}

	filePath := path.Join(outputDirectory, backup.BackupFileName+fileExtension)
	command := "mariadb-dump -h " + backup.Host + " -P " + strconv.Itoa(backup.Port) + " -u" + backup.User + " -p\"" + backup.Password + "\" --single-transaction " + databases + archiveMethod + "\"" + filePath + "\""

	if backup.DocerContainerName != "" {
		command = "docker exec " + backup.DocerContainerName + " " + command
	}

	log.Println("Exporting " + backup.Name + "(" + backup.Type + ")" + " backup to " + filePath)

	cmd := exec.Command("bash", "-c", command)
	stdout, err := cmd.Output()

	if err != nil {
		log.Print("failed")
		log.Fatalln(err.Error())
	}

	// Print the output
	log.Println(string(stdout))

	return backup.BackupFileName + fileExtension
}
