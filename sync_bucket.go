package main

import (
	"path"
	"slices"
	"strings"
)

func syncToBucket(backup BackupSetup, rootDir string) {
	filesInBucket := getObjectsInBucket(backup.Bucket, backup.Bucket.FilePrefix)
	filesInFolder := getFilesInFolder(path.Join(rootDir, backup.Name))

	deleteOldBucketFiles(backup, rootDir, filesInFolder, filesInBucket)
	uploadNewBucketFiles(backup, rootDir, filesInFolder, filesInBucket)
}

func uploadNewBucketFiles(backup BackupSetup, rootDir string, filesInFolder []string, filesInBucket []string) {

	for _, filePath := range filesInFolder {
		bucketFilePath := path.Join(backup.Bucket.FilePrefix, strings.Replace(filePath, path.Join(rootDir, backup.Name)+"/", "", 1))

		if !slices.Contains(filesInBucket, bucketFilePath) {
			uploadFileToBucket(backup.Bucket, filePath, bucketFilePath)
		}
	}
}

func deleteOldBucketFiles(backup BackupSetup, rootDir string, filesInFolder []string, filesInBucket []string) {
	var deleteFromBucket []string

	for _, filePath := range filesInBucket {
		localFilePath := path.Join(rootDir, backup.Name, strings.Replace(filePath, path.Base(backup.Bucket.FilePrefix)+"/", "", 1))

		if !slices.Contains(filesInFolder, localFilePath) {
			deleteFromBucket = append(deleteFromBucket, filePath)
		}
	}

	if len(deleteFromBucket) > 0 {
		deleteFilesFromBucket(backup.Bucket, deleteFromBucket)
	}
}
